from bs4 import BeautifulSoup 
import pandas as pd            
import re
import nltk

train = pd.read_csv("labeledTrainData.tsv", header=0,delimiter="\t", quoting=3)

print train.shape
print train.columns.values

# Initialize the BeautifulSoup object on a single movie review     
example1 = BeautifulSoup(train["review"][0])  

# Print the raw review and then the output of get_text(), for 
# comparison
print train["review"][0]
print example1.get_text()


# Use regular expressions to do a find-and-replace
letters_only = re.sub("[^a-zA-Z]"," ", example1.get_text() )          # The pattern to search for
                                         # The pattern to replace it with
                         # The text to search
print letters_only

lower_case = letters_only.lower()  
words = lower_case.split()

from nltk.corpus import stopwords # Import the stop word list
print stopwords.words("english") 

words = [w for w in words if not w in stopwords.words("english")]
print words

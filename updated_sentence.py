import pandas as pd
from bs4 import BeautifulSoup
import string
import re
import os
from nltk.corpus import stopwords
import nltk.data
import logging
import numpy as np  # Make sure that numpy is imported
from gensim.models import Word2Vec
from sklearn.ensemble import RandomForestClassifier


def makeFeatureVecTrain(review, model, counter,num_features):
    # Function to average all of the word vectors in a given
    # paragraph
    #
    # Pre-initialize an empty numpy array (for speed)
    featureVec = np.zeros((num_features,),dtype="float32")
    
    featureVec = model["SENT_"+str(int(counter))]
    starting = review_sentence_counter[counter]
    ending = review_sentence_counter[counter+1]
    for i in range(starting,ending):
        featureVec = np.add(featureVec,model["SENT_"+str(int(i))])
    featureVec = np.divide(featureVec,ending-starting)
    #
    # nwords = 0.
    #
    # Index2word is a list that contains the names of the words in
    # the model's vocabulary. Convert it to a set, for speed
    # index2word_set = set(model.index2word)
    #
    # Loop over each word in the review and, if it is in the model's
    # vocaublary, add its feature vector to the total
    # for word in words:
    #     if word in index2word_set:
    #         featureVec = np.add(featureVec,model[word])
    #         nwords = nwords + 1.
    #
    # Divide the result by the number of words to get the average
    # featureVec = np.divide(featureVec,nwords)
    return featureVec


def getAvgFeatureVecsTrain(reviews, model, num_features):
    # Given a set of reviews (each one a list of words), calculate
    # the average feature vector for each one and return a 2D numpy array
    #
    # Initialize a counter
    counter = 0
    #
    # Preallocate a 2D numpy array, for speed
    reviewFeatureVecs = np.zeros((len(reviews),num_features),dtype="float32")
    #
    # Loop through the reviews
    for review in reviews:
       #
       # Print a status message every 1000th review
       if counter%1000. == 0:
           print "Review %d of %d" % (counter, len(reviews))
       #
       # Call the function (defined above) that makes average feature vectors
       reviewFeatureVecs[counter] = makeFeatureVecTrain(review, model,counter ,num_features)
       #
       # Increment the counter
       counter = counter + 1
    return reviewFeatureVecs


def getAvgFeatureVecsTest(reviews, model, num_features):
    # Given a set of reviews (each one a list of words), calculate
    # the average feature vector for each one and return a 2D numpy array
    #
    # Initialize a counter
    counter = 0
    #
    # Preallocate a 2D numpy array, for speed
    reviewFeatureVecs = np.zeros((len(reviews),num_features),dtype="float32")
    #
    # Loop through the reviews
    for review in reviews:
       #
       # Print a status message every 1000th review
       if counter%1000. == 0.:
           print "Review %d of %d" % (counter, len(reviews))
       #
       # Call the function (defined above) that makes average feature vectors
       reviewFeatureVecs[counter] = makeFeatureVecTrain(review, model,counter ,num_features)
       #
       # Increment the counter
       counter = counter + 1
    return reviewFeatureVecs

def review_to_wordlist( review, remove_stopwords=False ):
    # Function to convert a document to a sequence of words,
    # optionally removing stop words.  Returns a list of words.
    #
    # 1. Remove HTML
    review_text = BeautifulSoup(review).get_text() #.decode('ascii', 'ignore')
    #  
    review_text=filter(lambda x : x in string.printable, review_text)
    # 2. Remove non-letters
    review_text = re.sub("[^a-zA-Z]"," ", review_text)
    #
    # 3. Convert words to lower case and split them
    words = review_text.lower().split()
    #
    # remove_stopwords = False
    # 4. Optionally remove stop words (false by default)
    if remove_stopwords:
        stops = set(stopwords.words("english"))
        words = [w for w in words if not w in stops]
    #
    # 5. Return a list of words
    return(words)


def getCleanReviewsTrain(reviews):
    clean_reviews = []
    t=0
    for review in reviews["review"]:
        clean_reviews.append( review_to_wordlist( review, remove_stopwords=True ))
        t=t+1
        if t>1000 : 
            break
    return clean_reviews

def getCleanReviews(reviews):
    clean_reviews = []
    for review in reviews["review"]:
        clean_reviews.append( review_to_wordlist( review, remove_stopwords=True ))
    return clean_reviews

def review_to_sentences( review, tokenizer, remove_stopwords=False ):
    # Function to split a review into parsed sentences. Returns a 
    # list of sentences, where each sentence is a list of words
    #
    # 1. Use the NLTK tokenizer to split the paragraph into sentences
    raw_sentences = tokenizer.tokenize(review.strip())
    #
    # 2. Loop over each sentence
    sentences = []
    for raw_sentence in raw_sentences:
        # If a sentence is empty, skip it
        if len(raw_sentence.strip()) > 0:
            # Otherwise, call review_to_wordlist to get a list of words
            if review_to_wordlist( raw_sentence, remove_stopwords) :
                sentences.append( review_to_wordlist( raw_sentence,remove_stopwords ))
    #
    # Return the list of sentences (each sentence is a list of words,
    # so this returns a list of lists
    return sentences





train = pd.read_csv( "labeledTrainData.tsv", header=0,delimiter="\t", quoting=3 ,encoding = 'utf8')
test = pd.read_csv( "testData.tsv", header=0, delimiter="\t", quoting=3,encoding = 'utf8' )
unlabeled_train = pd.read_csv( "unlabeledTrainData.tsv", header=0, delimiter="\t", quoting=3 ,encoding = 'utf8')

# train = pd.read_csv( os.path.join(os.path.dirname(__file__), 'data', 'labeledTrainData.tsv'), header=0, delimiter="\t", quoting=3 )
# test = pd.read_csv(os.path.join(os.path.dirname(__file__), 'data', 'testData.tsv'), header=0, delimiter="\t", quoting=3 )
# unlabeled_train = pd.read_csv( os.path.join(os.path.dirname(__file__), 'data', "unlabeledTrainData.tsv"), header=0,  delimiter="\t", quoting=3 )


# Verify the number of reviews that were read (100,000 in total)
print "Read %d labeled train reviews, %d labeled test reviews, " \
 "and %d unlabeled reviews\n" % (train["review"].size,
 test["review"].size, unlabeled_train["review"].size )



# Load the punkt tokenizer
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')



# ****** Split the labeled and unlabeled training sets into clean sentences
#
sentences = []  # Initialize an empty list of sentences

print "Parsing sentences from training set"
t = 0
review_sentence_counter=[]
review_sentence_counter.append(0)
initially = 0
for review in train["review"]:
    sentences += review_to_sentences(review, tokenizer)
    review_sentence_counter.append(initially)
    initially = len(sentences)
    t = t+1
    if t>1000:
        break

review_sentence_counter.append(len(sentences))
# iska matlab yeh hai ki review)sentence counter keeps track ki iski id ka review 
#(0 se start) ke sentences ki range kaha  se start joti hai
#

# print "Parsing sentences from unlabeled set"
# for review in unlabeled_train["review"]:
#     sentences += review_to_sentences(review, tokenizer)

from gensim.models import Doc2Vec
from gensim.models.doc2vec import LabeledLineSentence

# wfile = open('edit_new.txt','w')
from gensim.models.doc2vec import LabeledSentence

# for line in sentences:
#     wfile.write("%s\n" %line)

# wfile.close()
csentences=[]
counter=0
for line in sentences:
    label = ["SENT_" + str(int(counter))]
    # print label
    csentence = LabeledSentence(words=line, labels=label)
    csentences.append(csentence)
    counter = counter + 1
total = counter
# csentences = LabeledLineSentence('edit_new.txt')

# model1 = Doc2Vec(csentences, size=100, window=8, min_count=5, workers=4)
# model1.save('my_model_1stdttry.doc2vec')


print sentences[64]

model2 = Doc2Vec(size=300,alpha=0.025, min_alpha=0.025,min_count=1,workers=4,train_lbls=False,window=8)  # use fixed learning rate
model2.build_vocab(csentences)
 
for epoch in range(2):
    model2.train(csentences)
    model2.alpha -= 0.002  # decrease the learning rate
    model2.min_alpha = model2.alpha  # fix the learning rate, no decay


model2.save('my_model_2ndttry_mincount1.doc2vec')


lenSten = len(sentences)

# print "loading models , model3  and model4"
# model3 = Doc2Vec.load('my_model_1stdttry.doc2vec')
# model4 = Doc2Vec.load('my_model_2ndttry.doc2vec')

print "Creating average feature vecs for training reviews"

num_features=300
model4=model2
trainDataVecs = getAvgFeatureVecsTrain( getCleanReviewsTrain(train), model4, num_features )

print "Creating average feature vecs for test reviews"

testDataVecs = getAvgFeatureVecsTest( getCleanReviews(test), model4, num_features )


# ****** Fit a random forest to the training set, then make predictions
#
# Fit a random forest to the training data, using 100 trees
forest = RandomForestClassifier( n_estimators = 100 )

print "Fitting a random forest to labeled training data..."
forest = forest.fit( trainDataVecs, train["sentiment"] )

# Test & extract results
result = forest.predict( testDataVecs )

# Write the test results
output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )
output.to_csv( "Word2Vec_AverageVectors.csv", index=False, quoting=3 )
print "Wrote Word2Vec_AverageVectors.csv"